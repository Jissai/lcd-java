package company;

import java.util.Map;

class GridLetterConverter {

    private final Map<Character, GridLetter> letterMap = Map.of(
            '0', new GridLetter("._.", "|.|", "|_|"),
            '1', new GridLetter("...", "..|", "..|"),
            '2', new GridLetter("._.", "._|", "|_."),
            '3', new GridLetter("._.", "._|", "._|"),
            '4', new GridLetter("...", "|_|", "..|"),
            '5', new GridLetter("._.", "|_.", "._|"),
            '6', new GridLetter("._.", "|_.", "|_|"),
            '7', new GridLetter("._.", "..|", "..|"),
            '8', new GridLetter("._.", "|_|", "|_|"),
            '9', new GridLetter("._.", "|_|", "..|"));

    GridLetter fromChar(char toConvert) {
        return this.letterMap.getOrDefault(toConvert, GridLetter.Default);
    }

}
