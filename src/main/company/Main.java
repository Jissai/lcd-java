package company;

import java.util.Scanner;
import java.util.stream.Collectors;

class Main {

    private static final String EXIT_CONDITION = "quit";

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        GridLetterConverter converter = new GridLetterConverter();

        System.out.println(String.format("please enter the number(s) to display in LCD format. \"%s\" to exit ", EXIT_CONDITION));
        String input = in.nextLine();

        while (!EXIT_CONDITION.equalsIgnoreCase(input)) {

            GridComposition lineComposition = input.chars()
                    .mapToObj(c -> converter.fromChar((char) c))
                    .collect(Collectors.toCollection(GridComposition::new));
            lineComposition.display();
            input = in.nextLine();
        }

    }
}
