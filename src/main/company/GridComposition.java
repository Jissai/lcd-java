package company;

import java.util.ArrayList;

class GridComposition extends ArrayList<GridLetter> {

    void display() {

        this.forEach(l -> System.out.print(l.firstLine));
        System.out.println();
        this.forEach(l -> System.out.print(l.secondLine));
        System.out.println();
        this.forEach(l -> System.out.print(l.thirdLine));
        System.out.println();
    }
}
