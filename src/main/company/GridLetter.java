package company;

class GridLetter {

    static final GridLetter Default = new GridLetter("", "", "");

    final String firstLine;
    final String secondLine;
    final String thirdLine;

    GridLetter(final String firstLine, final String secondLine, final String thirdLine) {
        this.firstLine = firstLine;
        this.secondLine = secondLine;
        this.thirdLine = thirdLine;
    }

}
