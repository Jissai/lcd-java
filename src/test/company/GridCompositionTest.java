package company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

class GridCompositionTest {
    private ByteArrayOutputStream outContent;

    @BeforeEach
    void setUp() {
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    @Test
    @DisplayName("should display the Default GridLetter")
    void displaysAnEmptyGridLetter() {
        GridComposition composition = new GridComposition();
        composition.add(GridLetter.Default);
        composition.display();
        Assertions.assertEquals("\r\n\r\n\r\n", outContent.toString());
    }

    @Test
    @DisplayName("should display a GridLetter")
    void displaysAGridLetter() {
        GridComposition composition = new GridComposition();
        composition.add(new GridLetter("_", ".", " "));
        composition.display();
        Assertions.assertEquals("_\r\n.\r\n \r\n", outContent.toString());
    }

    @AfterEach
    void teardown() {
        System.setOut(System.out);
    }
}
