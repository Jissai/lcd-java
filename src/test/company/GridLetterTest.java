package company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class GridLetterTest {

    @Test
    @DisplayName("should return a valid GridLetter")
    void returnsAGridLetter() {

        GridLetter grid = new GridLetter("._.", "| |", "._.");

        Assertions.assertEquals("._.", grid.firstLine);
        Assertions.assertEquals("| |", grid.secondLine);
        Assertions.assertEquals("._.", grid.thirdLine);
    }
}
