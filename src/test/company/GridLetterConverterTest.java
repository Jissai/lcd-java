package company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class GridLetterConverterTest {

    private final GridLetterConverter gridLetterConverter = new GridLetterConverter();

    @ParameterizedTest(name = "{index} Should convert {0} to GridLetter")
    @CsvSource(value = {"2:._.:._|:|_.", "3:._.:._|:._|"}, delimiter = ':')
    void returnsGridLetterWhenCharIsValid(char toConvert, String firstLine, String secondLine, String thirdLine) {

        GridLetter result = gridLetterConverter.fromChar(toConvert);
        Assertions.assertEquals(firstLine, result.firstLine);
        Assertions.assertEquals(secondLine, result.secondLine);
        Assertions.assertEquals(thirdLine, result.thirdLine);
    }

    @Test
    void returnsEmptyGridLetterWhenCharIsInvalid() {
        Assertions.assertEquals(GridLetter.Default, gridLetterConverter.fromChar('a'));
    }

}
