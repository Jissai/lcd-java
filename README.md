Do it at home, not time-boxed, and with google access. 
 
Your task is to display numbers on an LCD screen. This screen is made of 3x3 grids where each slot may contain a dot ("."), an undescore ("_") or a pipe ("|").
Below are the translations for each number. 

```
._. ... ._. ._. ... ._. ._. ._. ._. ._.
|.| ..| ._| ._| |_| |_. |_. ..| |_| |_|
|_| ..| |_. ._| ..| ._| |_| ..| |_| ..|
```

Ex. 910
```
._. ... ._.
|_| ..| |.|
..| ..| |_|
```

Please provide a java command-line tool to display multiple numbers entered by an operator. 
Keep in mind that this code will be integrated within a larger project. Do it as if you were to deliver this code for a code review and production-ready ; For us that means each critical bug leads to heavy penalties.